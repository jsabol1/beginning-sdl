CSE 20212 Lab 8 Report
Jeff Sabol
March 28, 2013

One might say that the two most important parts of video game programming are
movement and collision detection.  Therefore, for this lab I chose to focus on
the latter while my partner made a simple program based on movement.  The
project displays 3 squares: 2 red ones, and 1 blue one.  The blue square moves
around with the cursor.  If it touches a red square, the red square will turn
green.  When the blue square leaves, the green square will become red again.
The user can click the mouse to exit the program.

For this project, 3 objects are declared: one for the moving square, and one
each for the stationary ones.  The same initial red sprite is used for both
stationary squares.  After initializing the objects, the program enters its
main loop, which first checks for events and then displays objects as
necessary.  The two events checked for are mouse movement and mouse clicking.
Mouse clicking simply exits the program, while mouse movement updates the
coordinates of the blue square so that its center corresponds to the cursor
position.  Collision detection with both stationary squares is also checked.
SDL rectangles correspond to the location of each square, and their vertices
are used to determine whether two squares are touching or not.  A green or red
sprite is then chosen for the stationary squares depending on the return value
of the collision detection.

Testing for this program was very extensive because I had to make sure that a
lot of SDL basics were working properly.  First, I made an image appear on the
screen using LazyFoo's test image.  Then, I tried putting the blue and
stationary squares in different locations.  Next, I tested movement for the
blue square.  Clearing the screen between frames was tricky, but I got around
this by rendering the black background before anything else.  Finally, I set
up collision detection and tested each square to make sure that it worked.  I
noticed that the green square would not revert back to red at first, so I
started testing for lack of collision as well.

Here is the time log for this project:

March 27, 16:00 - 17:00 -- Looked at LazyFoo's SDL tutorial.  Made image appear on screen
March 27, 18:30 - 19:30 -- Created sprites for blue, green and red squares.  Started reading SDL tutorials on moving image around
March 27, 21:00 - 22:30 -- Tested moving blue square with mouse as well as screen clearing between frames
March 28,  0:00 -  1:00 -- Placed red squares on screen.  Worked on making them turn green during collision with blue square
March 28, 11:00 - 11:30 -- Finalized lab, wrote report
March 28, 13:00 - 13:30 -- Set up Bitbucket account, posted code

The code, image files, and report for this project can be found at [URL].  In
addition, a rubric for next week's lab is posted in the lab9 directory.
