//collision.cpp
//Test program to demonstrate basic SDL knowledge and collision detection.
//A blue square is controlled with the cursor.  When it touches one of
//two red squares on the screen, the red square will turn green.
//When the blue square leaves the green one will turn red again.
//Click the mouse to exit this program.

//Jeff Sabol
//CSE 20212
//March 28, 2013

#include "SDL/SDL.h"

//Function to place a sprite at specific coordinates
void apply_surface(int x, int y, SDL_Surface* source, SDL_Surface* destination)
{
    SDL_Rect offset;
    offset.x = x;
    offset.y = y;
    SDL_BlitSurface( source, NULL, destination, &offset );
}

//Collision checking function.  Not too complicated,
//because the sprites being used are squares
bool check_collision(SDL_Rect A, SDL_Rect B)
{
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;

	leftA = A.x;
	rightA = A.x + A.w;
	topA = A.y;
	bottomA = A.y + A.h;

	leftB = B.x;
	rightB = B.x + B.w;
	topB = B.y;
	bottomB = B.y + B.h;

	if(bottomA <= topB)
		return false;
	if(topA >= bottomB)
		return false;
	if(rightA <= leftB)
		return false;
	if(leftA >= rightB)
		return false;

	return true;
}

int main()
{
	int x = 0;
	int y = 0;

	bool quit = false;
	
	//Open SDL
	SDL_Init(SDL_INIT_EVERYTHING);
	
	//Images
	SDL_Surface* blueSquare = NULL;
	SDL_Surface* redGreenSquare1 = NULL;
	SDL_Surface* redGreenSquare2 = NULL;
	SDL_Surface* background = NULL;

	SDL_Surface* screen = NULL;

	//SDL rectangles for collision detection
	SDL_Rect blueSquareRect;
		//x and y not declared here, since they are variable values
		blueSquareRect.w = 50;
		blueSquareRect.h = 50;

	SDL_Rect redGreenSquare1Rect;
		redGreenSquare1Rect.x = 100;
		redGreenSquare1Rect.y = 100;
		redGreenSquare1Rect.w = 50;
		redGreenSquare1Rect.h = 50;
	
	SDL_Rect redGreenSquare2Rect;
		redGreenSquare2Rect.x = 400;
		redGreenSquare2Rect.y = 300;
		redGreenSquare2Rect.w = 50;
		redGreenSquare2Rect.h = 50;

	SDL_Event event;
		
	//Set up screen
	screen = SDL_SetVideoMode(640, 480, 32, SDL_SWSURFACE);
	
	//Assign sprites
	blueSquare = SDL_LoadBMP("blueSquare.bmp");
	redGreenSquare1 = SDL_LoadBMP("redSquare.bmp");
	redGreenSquare2 = SDL_LoadBMP("redSquare.bmp");
	background = SDL_LoadBMP("background.bmp");

	//Main loop
	while(quit == false) {
		//Refresh screen by applying black background first
		apply_surface(0, 0, background, screen);
		//Event checking
		while(SDL_PollEvent(&event)) {
			switch (event.type) {
				//Mouse movement
				case SDL_MOUSEMOTION:
					//Assign center of blue square to cursor position
					x = event.motion.x - 25;
					y = event.motion.y - 25;
					blueSquareRect.x = x;
					blueSquareRect.y = y;
					break;
				
				//Mouse click
				case SDL_MOUSEBUTTONDOWN:
					//Quit
					quit = true;
					break;
			}
		}

		//Check collision between blue square and red/green squares.
		//If yes, color is green.  If no, color is red.
		//Draw the red/green squares in this step as well.
		
		//Square 1
		if(check_collision(blueSquareRect, redGreenSquare1Rect) == 1) {
			redGreenSquare1 = SDL_LoadBMP("greenSquare.bmp");
			apply_surface(100, 100, redGreenSquare1, screen);
		}
		else if(check_collision(blueSquareRect, redGreenSquare1Rect) == 0) {
			redGreenSquare1 = SDL_LoadBMP("redSquare.bmp");
			apply_surface(100, 100, redGreenSquare1, screen);
		}

		//Square 2
		if(check_collision(blueSquareRect, redGreenSquare2Rect) == 1) {
			redGreenSquare2 = SDL_LoadBMP("greenSquare.bmp");
			apply_surface(400, 300, redGreenSquare2, screen);
		}
		else if(check_collision(blueSquareRect, redGreenSquare2Rect) == 0) {
			redGreenSquare2 = SDL_LoadBMP("redSquare.bmp");
			apply_surface(400, 300, redGreenSquare2, screen);
		}

		//Draw blue square
		apply_surface(x, y, blueSquare, screen);

		//Update screen
		SDL_Flip(screen);

		//Delay sets FPS at close to 60
		SDL_Delay(16);
	}

	//Free images and exit
	SDL_FreeSurface(blueSquare);
	SDL_FreeSurface(redGreenSquare1);
	SDL_FreeSurface(redGreenSquare2);
	SDL_Quit();

	return 0;
}
